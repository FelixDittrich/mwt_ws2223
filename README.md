# MWT_WS2223

## Getting started
1. Build the Springboot application (skip tests and rename target jar to "user-mysql")
2. Run `docker build -t user-mysql .` to build docker image
3. Pull mysqpl image with `docker pull mysql:5.6`
3. Start mysql container with `docker run --name mysql-standalone -e MYSQL_ROOT_PASSWORD=password -e MYSQL_DATABASE=test -e MYSQL_USER=felix -e MYSQL_PASSWORD=password -d mysql:5.6`
4. Start app container with `docker run -d -p 8089:8089 --name user-mysql --link mysql-standalone:mysql user-mysql`

project is based on: https://medium.com/thecodefountain/develop-a-spring-boot-and-mysql-application-and-run-in-docker-end-to-end-15b7cdf3a2ba
