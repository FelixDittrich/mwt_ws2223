package com.hft.stuttgart.de.mwt.exceptions;

public class InvalidTransationReferenceException extends RuntimeException{

	private static final long serialVersionUID = 1L;
	
	public InvalidTransationReferenceException(String errorMessage) {
		super(errorMessage);
	}
}
